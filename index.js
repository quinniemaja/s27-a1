const http = require('http');

const port = 4000;

http.createServer(function(request, response) {
	if (request.url === "/") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to Booking System")

	} else if (request.url === "/profile" && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to your profile!")

	} else if(request.url === "/courses" && request.method === "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here's our courses available!")

	} else if(request.url === "/addCourse" && request.method === "POST") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add a course to our resources")

	} else if(request.url === "/updateCourse" && request.method === "PUT") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Update a course to our resources")

	} else if(request.url === "/archiveCourses" && request.method === "DELETE") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("archive courses to our resources")

	}


}).listen(port);

console.log(`Server is running at localHost:${port}`);